//
//  AppDelegate.h
//  ci-demo
//
//  Created by tnomg on 2019/5/24.
//  Copyright © 2019 tnomg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

